let latlong1 = '-75.668888';
let latlong2 = '45.4354055';

$("#pcSubmit").click(function(){
    let postalCode = $("#postCodeCatcher").val().replace(/\s/g, '').toUpperCase();
    let letters = /^[0-9a-zA-Z]+$/;

    if (postalCode.length == 6 && postalCode.match(letters)) {
        console.log(`${postalCode} is a valid postal code`);

        //Geocoding postal code
        //see National Resources Canada's geolocation service at https://www.nrcan.gc.ca/earth-sciences/geography/topographic-information/free-data-geogratis/geogratis-web-services/17216#g3
        // $.ajax({
        //     url:`http://geogratis.gc.ca/services/geolocation/en/locate?q=${postalCode}`,
        //     type: 'GET',
        //     dataType: 'jsonp',
        //     success(response){
        //         // console.log(response);

        //         latlong1 = response["0"].geometry.coordinates["0"];
        //         latlong2 = response["0"].geometry.coordinates["1"];

        //         console.log(latlong1);
        //         console.log(latlong2);
        //     }
        // });

        $.ajax({
            url: `https://represent.opennorth.ca/postcodes/${postalCode}`,
            // url: 'https://represent.opennorth.ca/representatives/?point=' + latlong2 + ',' + latlong1,
            type: 'GET',
            dataType: 'jsonp',
            success(response) {
                console.log(response);
                // console.log('https://represent.opennorth.ca/boundaries/?contains=' + latlong1 + ',' + latlong2)
                
                $('.AjaxResponseRow').empty();
                // $('body').append("<div class='row AjaxResponseRow'></div>");

                response.representatives_centroid.forEach(function(item){
                    $('.AjaxResponseRow').append(`<div class="col-xs-12 col-sm-4 col-md-3 representative"><div class="repPicture"><img src="${item.photo_url}" class="img-fluid"/></div><p>${item.party_name} ${item.elected_office}<a href="${item.url}"></a></p></div>`);
                });

                // response.objects.forEach(function(item){
                //     $('.AjaxResponseRow').append(`<div class="col-xs-12 col-sm-4 col-md-3 representative"><div class="repPicture"><img src="${item.photo_url}" class="img-fluid"/></div><p>${item.party_name} ${item.elected_office}<a href="${item.url}"></a></p></div>`);
                // });

                // SEE : https://stackoverflow.com/questions/9571056/bind-json-object-to-html-element
                // SEE : Google:"json data binding"
                // SEE : "knockout.js"
                // SEE : https://www.youtube.com/watch?v=HdFYtHJDGd8 
            },
            error(jqXHR, status, errorThrown) {
                alert('Sorry an error has occured');
            }
        });

    } else {
        alert("Please enter a valid postal code");
        $("#postCodeCatcher").val('');
    }
});
